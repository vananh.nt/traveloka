// hide the previous button
$('.previous').addClass('button-disabled');

$('.tabs li').click(function () {

    if ($(this).is(':last-child')) {
        $('.next').addClass('button-disabled');
    } else {
        $('.next').removeClass('button-disabled');
    }

    if ($(this).is(':first-child')) {
        $('.previous').addClass('button-disabled');
    } else {
        $('.previous').removeClass('button-disabled');
    }

    var position = $(this).position();
    var corresponding = $(this).data("id");

    // scroll to clicked tab with a little gap left to show previous tabs
    scroll = $('.tabs').scrollLeft();
    $('.tabs').animate({
        'scrollLeft': scroll + position.left - 30
    }, 200);


    // show content of corresponding tab
    $('div.' + corresponding).toggle();

    // remove active class from currently not active tabs
    $('.tabs li').removeClass('active');

    // add active class to clicked tab
    $(this).addClass('active');
});

$('div a').click(function(e){
    e.preventDefault();
    $('li.active').next('li').trigger('click');
});
$('.next').click(function(e){
    e.preventDefault();
    $('li.active').next('li').trigger('click');
});
$('.previous').click(function(e){
    e.preventDefault();
    $('li.active').prev('li').trigger('click');
});
